set dotenv-load

clean:
	find Games -not -name '*.md' -not -name 'PKGBUILD' -not -wholename './.git*' -not -name 'Justfile' -not -name 'description.xml' -type f -print0 | xargs -0 -n1 rm -v

package:
	find Games -maxdepth 1 -not -name 'Games' -type d -print0 | xargs -0 -n1 just _package

_package PACKAGE:
	#!/usr/bin/env bash
	set -e
	cd "{{PACKAGE}}"
	FILE=$(find . -name '*.zst')
	if [[ -z ${FILE} ]]; then
	  echo "Packaging {{PACKAGE}}"
	  updpkgsums >/dev/null || exit 255
	  makepkg -cf --skipchecksums --nodeps >/dev/null  || exit 255
	else
	  echo "Skipping {{PACKAGE}}"
	fi

deploy:
	rsync --rsync-path="sudo rsync" -avzr --inplace --progress Games/*/*.zst "$SSH_LOGIN@$SSH_HOST:$SSH_PATH"
	ssh $SSH_LOGIN@$SSH_HOST "sudo ${SCRIPT}"
